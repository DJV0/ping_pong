﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RabbitMQ.Wrapper.Services;
using RabbitMQ.Wrapper.Models;
using RabbitMQ.Client.Events;
using System.Threading;

namespace Pinger
{
    class Pinger
    {
        private QueueService _queue;
        public Pinger()
        {
            _queue = new QueueService("amqp://guest:guest@localhost:5672",
                new ConsumerSettings() { QueueName = "ping_queue" },
                new ProducerSettings() { QueueName = "pong_queue" });

            _queue.Consumer.Recieved += GetMessage;
        }

        private void GetMessage(object sender, BasicDeliverEventArgs e)
        {
            var body = e.Body.ToArray();
            var message = Encoding.UTF8.GetString(body);
            Console.WriteLine($"Time: {DateTime.Now}, Messege: {message}");

            _queue.SendMessageToQueue("ping");

        }

        public void Start()
        {
            _queue.ListenQueue();
            _queue.SendMessageToQueue("ping");
        }

        public void Dispose()
        {
            _queue.Dispose();
        }
    }
}
