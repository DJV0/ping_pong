﻿using System;
using System.Text;
using System.Threading;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using RabbitMQ.Wrapper.Services;
using RabbitMQ.Wrapper.Models;

namespace Pinger
{
    class Program
    {
        static void Main(string[] args)
        {
            Pinger pinger = new Pinger();
            pinger.Start();

            Console.WriteLine("Press [Enter] to exit");
            Console.ReadLine();

            pinger.Dispose();
        }
    }
}
