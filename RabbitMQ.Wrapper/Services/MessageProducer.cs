﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RabbitMQ.Client;
using RabbitMQ.Wrapper.Models;

namespace RabbitMQ.Wrapper.Services
{
    public class MessageProducer
    {
        private IModel _channel;
        private IConnection _connection;
        private ProducerSettings _settings;

        public MessageProducer(ConnectionFactory factory, ProducerSettings producerSettings)
        {
            _connection = factory.CreateConnection();
            _channel = _connection.CreateModel();
            _settings = producerSettings;
            _channel.QueueDeclare(queue: _settings.QueueName, durable: true, exclusive: false, autoDelete: false, arguments: null);
        }

        public void SendMessage(string message)
        {
            var body = Encoding.UTF8.GetBytes(message);
            var properties = _channel.CreateBasicProperties();
            properties.Persistent = true;
            _channel.BasicPublish(exchange: "", routingKey: _settings.QueueName, 
                basicProperties: properties, body: body);
        }

        public void Dispose()
        {
            _channel?.Dispose();
            _connection?.Dispose();
        }
    }
}
