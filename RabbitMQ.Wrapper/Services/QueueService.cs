﻿using System;
using RabbitMQ.Client;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RabbitMQ.Wrapper.Models;

namespace RabbitMQ.Wrapper.Services
{
    public class QueueService
    {
        public MessageConsumer Consumer { get; }
        public MessageProducer Producer { get; }
        private ConnectionFactory _factory;

        public QueueService(string connectionUri, ConsumerSettings consumerSettings, ProducerSettings producerSettings)
        {
            _factory = new ConnectionFactory() { Uri = new Uri(connectionUri) };
            Consumer = new MessageConsumer(_factory, consumerSettings);
            Producer = new MessageProducer(_factory, producerSettings);
        }

        public void ListenQueue()
        {
            Consumer.Connect();
        }

        public void SendMessageToQueue(string message)
        {
            Producer.SendMessage(message);
        }

        public void Dispose()
        {
            Consumer?.Dispose();
            Producer?.Dispose();
        }
    }
}
