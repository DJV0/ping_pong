﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using RabbitMQ.Wrapper.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RabbitMQ.Wrapper.Services
{
    public class MessageConsumer
    {
        private IModel _channel;
        private IConnection _connection;
        private EventingBasicConsumer _consumer { get; }
        private ConsumerSettings _settings;

        public event EventHandler<BasicDeliverEventArgs> Recieved
        {
            add => _consumer.Received += value;
            remove => _consumer.Received -= value;
        }

        public MessageConsumer(ConnectionFactory factory, ConsumerSettings consumerSettings)
        {
            _connection = factory.CreateConnection();
            _channel = _connection.CreateModel();
            _consumer = new EventingBasicConsumer(_channel);
            _settings = consumerSettings;
        }

        public void Connect()
        {
            _channel.QueueDeclare(_settings.QueueName, durable: true, exclusive: false, autoDelete: false, arguments: null);
            _channel.BasicConsume(_settings.QueueName, _settings.AutoAcknowledge, _consumer);
        }

        public void Dispose()
        {
            _channel?.Dispose();
            _connection?.Dispose();
        }
    }
}
