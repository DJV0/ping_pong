﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RabbitMQ.Client;

namespace RabbitMQ.Wrapper.Models
{
    public class ProducerSettings
    {
        public string QueueName { get; set; }
    }
}
