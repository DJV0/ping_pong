﻿using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RabbitMQ.Wrapper.Models
{
    public class ConsumerSettings
    {
        public string QueueName { get; set; }
        public bool AutoAcknowledge { get; set; } = true;
    }
}
