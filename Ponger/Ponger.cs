﻿using RabbitMQ.Client.Events;
using RabbitMQ.Wrapper.Models;
using RabbitMQ.Wrapper.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Ponger
{
    class Ponger
    {
        private QueueService _queue;
        public Ponger()
        {
            _queue = new QueueService("amqp://guest:guest@localhost:5672",
                new ConsumerSettings() { QueueName = "pong_queue" },
                new ProducerSettings() { QueueName = "ping_queue" });

            _queue.Consumer.Recieved += GetMessage;
        }

        private void GetMessage(object sender, BasicDeliverEventArgs e)
        {
            var body = e.Body.ToArray();
            var message = Encoding.UTF8.GetString(body);
            Console.WriteLine($"Time: {DateTime.Now}, Messege: {message}");

            Thread.Sleep(2500);

            _queue.SendMessageToQueue("pong");
        }

        public void Start()
        {
            _queue.ListenQueue();
        }

        public void Dispose()
        {
            _queue.Dispose();
        }
    }
}
