﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Text;
using System.Threading;

namespace Ponger
{
    class Program
    {

        static void Main(string[] args)
        {

            Ponger ponger = new Ponger();
            ponger.Start();

            Console.WriteLine("Press [Enter] to exit");
            Console.ReadLine();

            ponger.Dispose();
        }
    }
}
